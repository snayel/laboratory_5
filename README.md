Name: Mathias Hansen
Email: Mhans10@u.rochester.edu
Date: 10/13/2020
Academic honesty: I affirm that I have not given or received any unauthorized help on this laboratory assignment, and that all work is my own.
Description: This laboratory exercise included using code from a previous lab for display driver code to manage and write data to the LCD display on the explorer 16/32 dev board. The main.c source file has code to read analog data from the board's potentiometer which is converted to digital data and output through the LCD display and LEDs on the board.